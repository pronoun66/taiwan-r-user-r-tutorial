toJSON.data.frame <- function(input) {
	if (!require(rjson)) return(NULL)
# 	if (!require(devtools)) return(NULL)
# 	source("remove_unicode_escape.R")
# 	source("hex2str.R")
	row.data <- list()
	for(i in 1:nrow(input)) {
		row.element <- list()
		j <- 0
		for(name in names(input)) {
			row.element[j <- j+1] <- as.character(input[[name]][i])
		}
		row.data[[length(row.data) + 1]] <- row.element
	}
	retval <- list()
	retval[["table"]] <- list()
	retval[["table"]][["cols"]] <- names(input)
	retval[["table"]][["rows"]] <- row.data
# 	return(remove_unicode_escape(toJSON(retval)))	
	return(toJSON(retval, "R"))
}