% R Data Import / Export
% Taiwan R User Group / Wush Wu
% 2013-05-20

```{r, include=FALSE}
opts_chunk$set(fig.cap='')
opts_knit$set(upload.fun = image_uri)
gvis.opt <- list(
   alternatingRowStyle=TRUE,
 	page='enable'
)
# options(GoogleDocsPassword = c('wush978' = 'xxx'))
source("auth.R")
library(ggplot2)
```


# 

<center>
<table><tr><td>
## About Me
</td><td>
## Outline
</td></tr><tr><td>
- One of Host of Taiwan R User Group
- Use R since 2008.
- Publication of R
    - Author of R package: RMessenger
    - Contributor of knitr
</td><td>
- Introduction
- R Native Data
- SQL
- NoSQL
- Web
- Some existed package of Web API
- Q&A
</td></tr></table>
</center>
		
# Data Analysis

<table><tr><td>
##Data Preparation
</td><td>
## Export Result of Analysis
</td>
</tr><tr>
<td>
<http://horicky.blogspot.com.au/2012/05/predictive-analytics-data-preparation.html>

- Sample Data
- Impute missing data
- Normalization
- Dimension Reduction
- Add derived attributes
- Descretize Numeric Value into Categories
- Binarize Categorical Data
- Select, Combine, Aggregate Data
- Power and Log Transformation

**We need to import the data first**
</td><td>
- Make Conclusion
- Summarize Analysis Result
- Produce Plots
- Writing Report

**We need to export the result of analysis**
</td></tr></table>

# Source of Data

<center>
<table><tr><td>
- Native R Files
- Database 
- System Files
- Web Page
- Web API
- Data Streams
</td><td>
<image src="`r image_uri("img/database-developer-Eastbourne-Brighton-Sussex.png")`" width="361" height="400"/>
</td></tr></table>
</center>

# Native R Files

<table><tr><td>
## Functions
</td><td>
## Example
</td></tr><tr><td>
- `save`, `load`, `save.image`: `name` and `object`
- `saveRDS`, `readRDS`: `object`
- `serialize`, `unserialize`: in memory
- `dump`: text representations
</td><td>
```{r}
a <- list(a=1, b=2)
save(a, file="a.Rdata")
rm("a")
a
load("a.Rdata")
a
```
</td></tr></table>

# Database

<center>
<table><tr><td>
## Database Client

- DBI <http://cran.r-project.org/web/packages/DBI/index.html>
- [rmongodb](http://cran.r-project.org/web/packages/rmongodb/index.html), [rredis](http://cran.r-project.org/web/packages/rredis/index.html)...
- External Tools
</td></tr>
</table>
</center>

# DBI - A Generalized Database Interface for R

<center>
<table><tr><td>
- Connection: `dbConnect`, `dbDisconnect`
- `dbListTables`, `dbListFields`
- CRUD:`dbWriteTable`, `dbReadTable`, `dbRemoveTable`
- SQL:`dbSendQuery`, `fetch`, `dbClearResult`
- No Transaction
</td><td>
<iframe src="http://cran.r-project.org/web/packages/DBI/index.html" width="400" height="400"></iframe>
</td></tr></table>
</center>

# RSQLite - DBI Interface

```{r, results='hide', eval=FALSE}
suppressPackageStartupMessages(library(RSQLite))
db <- dbConnect("SQLite", "iris.db")
dbReadTable(db, "iris")
dbDisconnect(db)
```

```{r, echo=FALSE, results='asis'}
suppressPackageStartupMessages(library(RSQLite))
suppressPackageStartupMessages(library(googleVis))
db <- dbConnect("SQLite", "iris.db")
m <- gvisTable(dbReadTable(db, "iris"), 
 options=gvis.opt
)
cat("<center>")
cat(m$html$chart)
cat("</center>")
invisible(dbDisconnect(db))
```

# RSQLite - Transaction and Other Features

- Transaction
- SQL Preparation

```{r, results='hide', eval=FALSE}
library(RSQLite)
db <- dbConnect("SQLite", "cars.db")
if (dbExistsTable(db, "cars")) dbRemoveTable(db, "cars")
dbBeginTransaction(db)
sql.create <- "create table cars (speed INTEGER, dist INTEGER)"
sql.insert <- "insert into cars values (:speed, :dist)"
dbGetPreparedQuery(db, sql.create, bind.data = cars)
dbGetPreparedQuery(db, sql.insert, bind.data = cars)
dbCommit(db)
dbReadTable(db, "cars", row.names=FALSE)
dbDisconnect(db)
```

```{r, echo=FALSE, results='asis'}
suppressPackageStartupMessages(library(RSQLite))
suppressPackageStartupMessages(library(googleVis))
db <- dbConnect("SQLite", "cars.db")
if (dbExistsTable(db, "cars")) invisible(dbRemoveTable(db, "cars"))
invisible(dbBeginTransaction(db))
sql.create <- "create table cars (speed INTEGER, dist INTEGER)"
sql.insert <- "insert into cars values (:speed, :dist)"
invisible(dbGetPreparedQuery(db, sql.create, bind.data = cars))
invisible(dbGetPreparedQuery(db, sql.insert, bind.data = cars))
invisible(dbCommit(db))
m <- gvisTable(dbReadTable(db, "cars", row.names=FALSE), options=gvis.opt)
cat("<center>")
cat(m$html$chart)
cat("</center>")
invisible(dbDisconnect(db))
```

# RSQLite - Batch Loading

- For loading Big Data
- Performance Issue:
    - Don't exhaust the memory
    - Allocate the result and modify in place. **data.table: a package for in-memory modification and indexing of data.frame**

```{r, eval=FALSE}
# This is a script to load 10^7 rows into R in several minutes
library(data.table)
library(bit64) # A package for 64bit integer in R.
library(RSQLite)

db.pv <- try(dbConnect("SQLite", normalizePath(sprintf("CountPV/%s/pv.db", param.date), mustWork=TRUE)), silent=TRUE)
if (class(db.pv) == "try-error") {
  # Error Handling
}

res <- dbSendQuery(db.pv, "SELECT COUNT(*) FROM ExampleTableName")
n <- fetch(res)
dbClearResult(res)
pv <- data.table(example_column1=rep(as.integer64(0), n), example_column2=rep("", n), example_column3=rep(0L, n))
gc()

res <- dbSendQuery(db.pv, "SELECT example_column1, example_column2, example_column3 FROM ExampleTableName")
pv.part <- fetch(res, m)
i <- 1
pb <- txtProgressBar(max = n)
while(nrow(pv.part) > 0) {
  pv.part$example_column1 <- as.integer64(pv.part$example_column1)
  pv.part$example_column3 <- as.integer(pv.part$example_column3)
  pv.part
  pv[seq(i, by=1, length=nrow(pv.part)), example_column1 := pv.part$example_column1]
  pv[seq(i, by=1, length=nrow(pv.part)), example_column2 := pv.part$example_column2]
  pv[seq(i, by=1, length=nrow(pv.part)), example_column3 := pv.part$example_column3]
  i <- i + nrow(pv.part)
  setTxtProgressBar(pb, i - 1)
  pv.part <- fetch(res, m)
}
close(pb)
dbClearResult(res)
dbDisconnect(db.pv)
```

# External Client: 

<table><tr><td>
- Command Line Tools
- External Client: Integrate R with Python, Java
    - [RembedPy](https://github.com/wush978/RembedPy): An experimental intergration of R and Python
    - [Solrr](https://github.com/wush978/solrr): An experimental client to insert data into [SolrCloud](http://wiki.apache.org/solr/SolrCloud) with rJava.
</td><td>

```r
library(RembedPy)

pyscript("import cql")
cql <- pyobj("cql")
con <- cql$connect("localhost", cql_version="3.0.0") 
# cql_version 3.0.0 is unavailable in R
cursor <- con$cursor()
cursor$execute("use keyspace_name")
cursor$execute("SELECT * FROM column_family_name LIMIT 2")
cursor["description"]
pyscript("
def test_print(src):
    print src
")
pycall("test_print", cursor["description"])
result <- cursor$fetchone()
as.list(result)
```

</td></tr></table>

# NoSQL

<table><tr><td>
Key-Value stores

Only list some packages which I used before:

- [mongodb]()
- [rredis]()
</td><td>

## An example of `rredis`

```{r rredis}
library(rredis)
redisConnect()
redisSet("a", list(a=1,b=2))
redisGet("a")
redisClose()
```
</td></tr>
</table>

# System File: General Issue

<table><tr><td>
## Encoding and [BOM](http://en.wikipedia.org/wiki/Byte_order_mark)

Before importing a txt file, you need to tell R which encoding the file is using.

- `file` command-line tool (for Windows, included in Rtools)
    
```
$ file utf8.txt 
utf8.txt: UTF-8 Unicode text
$ file utf8-BOM.txt 
utf8-BOM.txt: UTF-8 Unicode (with BOM) text
$ file big5.txt
big5.txt: ISO-8859 text, with CRLF line terminators
```

- In some complicated situation (web log with URL), I need to use other tool such as python to handle the encoding issue.

## Unicode Escape

- I faced the issue of unicode escape when using package `JSONIO`. <http://stackoverflow.com/questions/9223795/how-to-correctly-deal-with-escaped-unicode-characters-in-r-e-g-the-em-dash>

</td><td>
## `iconv`

```{r}
readLines("utf8.txt")
readLines("utf8-BOM.txt")
readLines("big5.txt")
iconv(readLines("big5.txt"), from="big5", to="utf8")
```
</td></tr></table>

# Common Format: 

Only list some packages which I used before:

- `csv`: `read.csv`, `write.csv`. Check `quote` and `sep`
- `xls`: `read.xls`(in package `xlsReadWrite` or `gdata`), `write.xls`(in package `xlsReadWrite` or `dataframes2xls`)
- `xlsx`: `read.xlsx`, `write.xlsx` in package `xlsx`
- `ods`: `read.ods` in package `ROpenOffice`
- `foreign`: Read Data Stored by Minitab, S, SAS, SPSS, Stata, Systat, dBase, ... 

**Always check the file before importing**

# Web Related

<table><tr><td>
## Web Page: `spider`(Crawler)
</td><td>
## Common Format:
</td>
</tr><tr>
<td>
<iframe width="560" height="315" src="http://www.youtube.com/embed/P3Xm_JFmh04?list=PLM7HGQkDNOHtUmjpIuO-aOJ0Rhj0sxT9B" frameborder="0" allowfullscreen></iframe>
</td><td>
- XML: package `XML`
- JSON: package `JSONIO`, `rjson`
</td></tr></table>

# Example: XML

## How many package does CRAN have?

```{r pkg-num, cache=TRUE}
suppressPackageStartupMessages(library(XML))
r_pkg <- "http://cran.rstudio.com/web/packages/available_packages_by_date.html"
tb <- readHTMLTable(r_pkg)[[1]]
nrow(tb)
```

## Distribution of last updated date

```{r pkg-submission}
tb.date <- as.Date(as.character(tb[[" Date "]]))
range(tb.date)
pie(table(findInterval(as.integer(tb.date), vec=as.integer(seq.Date(from=as.Date("2005-01-01"), to=as.Date("2014-01-01"), by="years")))), labels=format(seq.Date(from=as.Date("2005-01-01"), to=as.Date("2013-01-01"), by="years"), "%Y"))
```

# Web API: 

Only list some resources which I awared before:

- Maps: `OpenStreetMap`, `ggmap`, `osmar`
- GoogleDocs: `RGoogleDocs`
- Twitter: `twitteR`

# Map

<http://stackoverflow.com/questions/11056738/plotting-points-from-a-data-frame-using-openstreetmap>

Add hotspot of wifi (from <http://data.taipei.gov.tw/opendata/apply/NewDataContent?oid=094E4C4E-2E35-4B76-BDBB-F842DC0A865B>) to the map:

```{r, cache=TRUE}
library(ggmap)
wifi <- read.csv("wifi-utf8.csv") # I manually convert the encoding before importing!!
lng <- c(121.5138, 121.5785)
lat <- c(25.0233, 25.069)
index.lng <- wifi$lng > lng[1] & wifi$lng < lng[2]
index.lat <- wifi$lat > lat[1] & wifi$lat < lat[2]
wifi <- wifi[index.lng & index.lat,]
if (file.exists("map.RDS")) {
  map <- readRDS("map.RDS")
} else {
  map <- get_map(c(min(lng), min(lat), max(lng), max(lat)), source="osm", maptype="roadmap")
}
ggmap(map)
ggmap(map) + geom_point(data=wifi, aes(x=lng, y=lat), size=1)
```


# Google Doc

```{r, echo=TRUE, eval=FALSE}
suppressPackageStartupMessages(library(RGoogleDocs))
# options(GoogleDocsPassword = c('login id' = 'password')
con <- getGoogleDocsConnection(service="wise")
sheets <- getWorksheets("TW.R COSCUP 講題投票 (回覆內容)", con)
result <- sheetAsMatrix(sheets[[1]], header=TRUE)[,2:31]
m <- gvisTable(result, 
 options=gvis.opt
)
cat(m$html$chart)
```

<center>
```{r google-docs, echo=FALSE, eval=TRUE, cache=TRUE, results='asis'}
suppressPackageStartupMessages(library(RGoogleDocs))
# options(GoogleDocsPassword = c('login id' = 'password')
con <- getGoogleDocsConnection(service="wise")
sheets <- getWorksheets("TW.R COSCUP 講題投票 (回覆內容)", con)
result <- sheetAsMatrix(sheets[[1]], header=TRUE)[,2:31]
m <- gvisTable(result, 
 options=gvis.opt
)
cat(m$html$chart)
```
</center>

# Twitter

An example from <http://cran.r-project.org/web/packages/twitteR/twitteR.pdf>

Comparing the tweets from Taipei and Maynila relating to **fisherman**:

```{r, cache=TRUE, results='asis'}
suppressPackageStartupMessages(library(twitteR))
suppressPackageStartupMessages(library(xtable))
source("searchTwitter.R")
cred <- readRDS("cred.Rds")
invisible(registerTwitterOAuth(cred))

tw_Tweets <- searchTwitter("fisherman", n=300, lang="en", since="2013-05-16", geocode="25.0233,121.5784,300km")#, 25.0233,121.5784,300km)
tw_sources <- sapply(tw_Tweets, function(x) x$text)
length(tw_sources) <- 300
ph_Tweets <- searchTwitter("fisherman", n=300, lang="en", since="2013-05-16", geocode="14.3459,120.5800,300km")
ph_sources <- sapply(ph_Tweets, function(x) x$text)
result <- data.frame(tw=tw_sources, ph=ph_sources)
result$tw <- iconv(result$tw, from='utf-8', to='ascii')
result$ph <- iconv(result$ph, from='utf-8', to='ascii')
print(xtable(result), type="html")
```

# Data Streams

Check our incoming talk at 6/24 by Philips: about collecting stock data stream with rJava. It should be a great example of importing data streams.

## Call for Proposal

- July: Visualization
- August: Algorithm of Machine Learning

Please send your proposal to <taiwan.user.group@gmail.com>

# Q&A
