library(shiny)

# Define UI for application that plots random distributions 
shinyUI(pageWithSidebar(
  
  # Application title
  headerPanel("Plot!"),
  
  # Sidebar with a slider input for number of observations
  sidebarPanel(
    #     sliderInput("date", "Date:", 
    #                 min = 0, max = 400, value = 200),
    textInput("type", "type:(p,l,b,s,o,S,h,n)", "o"),
    textInput("bty", "bty:(o,l,7,c,u,])", "o"),
    numericInput("pch", "pch: (integer: 0-127)", 11),
    numericInput("lty", "lty: (integer: 0-6)", 1),
    numericInput("cex", "cex: (positive number)", 1),
    sliderInput("lwd", "lwd:(positive number)", 
                min = 0.25, max = 5, value = 1,step=0.25),
    textInput("col", "col:(integer or color name)", "blue"),
    
    sliderInput("xlim", "xlim & ylim: c(number,number)", 
                min = 1, max = 5, value = 1),
    
    
    textInput("log", "log:('x','y','xy')", ""),
    textInput("main", "main:", "Test"),
    textInput("xlab", "xlab:", "X"),
    textInput("ylab", "ylab:", "Y"),
    radioButtons("ann", "ann:",
                 list("True" = "True",
                      "False" = "False")),
    radioButtons("axes", "axes:",
                 list("True" = "True",
                      "False" = "False")),
    radioButtons("frame.plot", "frame.plot:",
                 list("True" = "True",
                      "False" = "False"))
  ),
  
  # Show a plot of the generated distribution
  mainPanel(
    plotOutput("distPlot") , 
    div(plotOutput(outputId = "text"))
  )
))
