library(shiny)
library(grid)
# Define server logic required to generate and plot a random distribution
shinyServer(function(input, output) {
  
#   methodInput <- reactive({
#     switch(input$methods,
#            "BBands" = "addBBands()",
#            "SMI" = "addSMI()",
#            "SAR" = "addSAR()")
#   })

  
  output$distPlot <- renderPlot({
    plot(iris[,3],type=input$type,bty=input$bty,pch=input$pch,lty=input$lty,cex=input$cex,lwd=input$lwd,col=input$col,xlim=c(0+input$xlim*1,150-input$xlim*10),ylim=c(0.1,7+input$xlim),main=input$main,log=input$log,xlab=input$xlab,ylab=input$ylab,ann=input$ann,axes=input$axes,frame.plot=input$frame.plot)

  })
  output$text <- renderPlot({
    plot(1:10,ann=F,frame.plot=F,type="n",axes=F)
    grid.text(
      'plot(x, y,type="p",    bty="o",     pch =1,
                 lty =1,      cex =1,      lwd =1,
                 asp=1,       xlim=c(1,1), ylim=c(1,1),
                 log = "",    main= NULL,  sub = NULL,
                 xlab= "X",   ylab= "Y",   col=1
                 cex.main =1, col.lab =1,  font.sub =1,
                 ann = TRUE,  axes = TRUE, frame.plot = TRUE,
                 panel.first = NULL,)
      ',0.25,0.5,gp=gpar(col='red',cex=1.5),just="left")
  })
  
})
